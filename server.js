const { createServer } = require('http');
const PORT = process.env.PORT || 3000;
const HOSTNAME = process.env.HOSTNAME || 'localhost';

const requestListener = function requestListener(request, response) {
    response.end('Smart API');
};
const server = createServer(requestListener);

server.on('request', (request) => {
    const { method, url, headers } = request;

    console.log(`${ method } ${ url } ${ headers['user-agent'] }`);
});

server.listen(PORT, HOSTNAME, function () {
    console.log(`Server is listening on http://${ HOSTNAME }:${ PORT }`);
});