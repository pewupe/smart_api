# Smart API

API for creating and retrieving info about our sensors, controllers and other IoThings

## Technical stack
- [Git](https://git-scm.com/) version control
- [Node JS](https://nodejs.org/en/) version above 12
- [NPM (Node Package Manager)](https://docs.npmjs.com/)

## Project setup
1. You should have all things from above installed on your system
2. Type in your terminal: `npm install`

## Running project
1. Type in your terminal: `npm start`
2. In your favorite browser, open URI which should appear in terminal after start task